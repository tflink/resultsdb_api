%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib(1))")}
%endif

Name:           resultsdb_api
Version:        1.1.0
Release:        1%{?dist}
Summary:        Interface api to ResultsDB

License:        GPLv2+
URL:            https://bitbucket.org/fedoraqa/resultsdb_api
Source0:        https://qadevel.cloud.fedoraproject.org/releases/%{name}/%{name}-%{version}.tar.gz

BuildArch:      noarch

Requires:       python-requests
BuildRequires:  python2-devel python-setuptools

%description
Interface api to ResultsDB

%prep
%setup -q

%build
%{__python2} setup.py build

%install
%{__python2} setup.py install --skip-build --root %{buildroot}

%files
%doc README.md
%{python_sitelib}/resultsdb_api.*
%{python_sitelib}/*.egg-info

%changelog
* Fri May 16 2014 Tim Flink <tflink@fedoraproject.org> - 1.1.0-1
- Releasing resultsdb_api 1.1.0

* Fri Apr 25 2014 Tim Flink <tflink@fedoraproject.org> - 1.0.2-1
- bugfixes from upstream

* Fri Apr 11 2014 Tim Flink <tflink@fedoraproject.org> - 1.0.1-1
- initial packaging
